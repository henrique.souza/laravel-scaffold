<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});

Auth::routes([
    'register' => false,
]);

Route::middleware(['auth', 'notifications'])->group(function () {

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/exemplo-1', function () {
        return view('exemplo', [
            'nome_funcionalidade' => 'Exemplo 1'
        ]);
    });
});

/**
 * TODO - Mover para arquitetura
 */
/**
 * Ajax Routes
 */
Route::middleware('auth')->group(function () {
    Route::get('notificacoes/total-nao-lidas', 'Components\NotificacaoController@getTotalNaoLidas');
    Route::get('notificacoes', 'Components\NotificacaoController@get');
    Route::put('notificacoes/{cdNotificacao}/{action}', 'Components\NotificacaoController@changeStatus');
});