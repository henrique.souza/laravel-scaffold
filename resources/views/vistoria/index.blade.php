@extends('adminlte::page')

@section('title', 'Vistoria')

@section('content_header')
    <h1>Vistoria</h1>
@stop

@section('content')

    {{ Form::open(['url' => 'vistorias', 'method' => 'get']) }}

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Pesquisar</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar"
                        data-original-title="Collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <div class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="ds_despacho">Relatório</label>
                                <input name="ds_despacho" id="ds_despacho" class="form-control form-control-sm"
                                       type="text" placeholder="Relatório">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="btn-toolbar">
                        <button type="submit" class="btn btn-primary">Pesquisar</button>
                        <a href="{{ route('vistoria.create') }}" role="button" class="btn btn-primary">Novo</a>
                    </div>
                </div>
            </div>
        </div>
        <!--div class="box-footer">
        </div-->
    </div>

    {{ Form::close() }}

    <div class="box">
        <div class="box-body">
            <table id="lista-vistorias" class="table table-bordered table-striped" style="width:100%">
                <thead>
                <tr>
                    <th>Código</th>
                    <th>Despacho</th>
                    <th>Interessado</th>
                    <th>Data</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Código</th>
                    <th>Despacho</th>
                    <th>Interessado</th>
                    <th>Data</th>
                    <th>Ações</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection

@section('js')

    <script>

        $(document).ready(() => {
            $('#lista-vistorias').DataTable({
                "ajax": {
                    "url": '{{ route('vistorias.list') }}',
                    "cache": true
                },
                columns: [
                    { data: 'cd_vistoria', "className": 'text-center' },
                    { data: 'ds_despacho' },
                    { data: 'nm_interessado' },
                    { data: 'dt_vistoria', "className": 'text-center' },
                    {
                        data: (item) =>
                            `<input type="hidden" value="${item.cd_vistoria}">
                            <a href="{{ route('vistoria.index') }}/${item.cd_vistoria}/edit" class="btn btn-data-table btn-dt-edit" title="Editar">
                                <i class="fas fa-edit"></i>
                            </a>
                            <a class="btn btn-data-table btn-dt-remove" title="Excluir">
                                <i class="fas fa-trash"></i>
                            </a>`
                        ,
                        "className": 'text-center'
                    },
                ],
                responsive: true,
                language: {
                    url: 'components/pt_br/dataTables.json'
                }
            }).on("draw", () => {
                $(".btn-dt-remove").click(remover);
            });
        });

        let remover = (event) => {
            console.log(event.target);
        }

    </script>

@endsection
