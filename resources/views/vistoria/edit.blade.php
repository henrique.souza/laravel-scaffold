@extends('adminlte::page')

@section('title', 'Vistoria')

@section('content_header')
    <h1>Editar Vistoria</h1>
@stop

@section('content')

    {{ Form::open(['url' => route('vistoria.update', $model->cd_vistoria), 'method' => 'put']) }}
    {{ Form::token() }}

    <div class="box">
        <div class="box-body">
            <div class="panel">
                <div class="panel-heading">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @elseif(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group @error('dt_vistoria') has-feedback has-error @enderror">
                                {{ Form::label('dt_vistoria', 'Data') }}
                                {{ Form::date('dt_vistoria', $model->dt_vistoria, ['class' => 'form-control form-control-sm', 'placeholder' => 'Data da Vistoria']) }}
                                @error('dt_vistoria')
                                <span class="help-block">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                {{ Form::label('tp_documento', 'Tipo') }}
                                {{ Form::text('tp_documento', $model->tp_documento, ['class' => 'form-control form-control-sm', 'placeholder' => 'Tipo da Vistoria']) }}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                {{ Form::label('nm_interessado', 'Interessado') }}
                                {{ Form::text('nm_interessado', $model->nm_interessado, ['class' => 'form-control form-control-sm', 'placeholder' => 'Nome do Interessado']) }}
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                {{ Form::label('ds_despacho', 'Despacho') }}
                                {{ Form::text('ds_despacho', $model->ds_despacho, ['class' => 'form-control form-control-sm', 'placeholder' => 'Descrição do Despacho']) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="btn-toolbar">
                        {{ Form::submit('Gravar', ['class' => 'btn btn-primary']) }}
                        {{ Html::link(route('vistoria.index'), 'Voltar', ['class' => 'btn btn-primary']) }}
                    </div>
                </div>
            </div>
        </div>
        <!--div class="box-footer">
        </div-->
    </div>

    {{ Form::close() }}

@endsection