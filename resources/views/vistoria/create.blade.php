@extends('adminlte::page')

@section('title', 'Vistoria')

@section('content_header')
    <h1>Nova Vistoria</h1>
@stop

@section('content')

    {{ Form::open(['url' => 'vistorias', 'method' => 'post']) }}

    <div class="box">
        <div class="box-body">
            <div class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="dt_vistoria">Data</label>
                                <input name="dt_vistoria" id="dt_vistoria" class="form-control form-control-sm" type="date" placeholder="Relatório">
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="tp_documento">Tipo Documento</label>
                                <input name="tp_documento" id="tp_documento" class="form-control form-control-sm" type="text" placeholder="Relatório">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="nm_interessado">Interessado</label>
                                <input name="nm_interessado" id="nm_interessado" class="form-control form-control-sm" type="text" placeholder="Relatório">
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="tp_vistoria">Tipo Vistoria</label>
                                <input name="tp_vistoria" id="tp_vistoria" class="form-control form-control-sm" type="text" placeholder="Relatório">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="ds_despacho">Relatório</label>
                                <input name="ds_despacho" id="ds_despacho" class="form-control form-control-sm" type="text" placeholder="Relatório">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="btn-toolbar">
                        <button type="submit" class="btn btn-primary">Gravar</button>
                    </div>
                </div>
            </div>
        </div>
        <!--div class="box-footer">
        </div-->
    </div>

    {{ Form::close() }}

@endsection