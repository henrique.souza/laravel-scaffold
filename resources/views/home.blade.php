<?php
// TODO - Mover para arquitetura
?>
@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Home</h1>
@stop

@section('css')
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop

@section('content')
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Dashboard</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar" data-original-title="Collapse">
                <i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="panel">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <div class="info-box">
                            <!-- Apply any bg-* class to to the icon to color it -->
                            <span class="info-box-icon bg-red"><i class="fa fa-star"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Likes</span>
                                <span class="info-box-number">93,139</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </div>
                    <div class="col-sm-6 col-xs-12">
                        <!-- /.info-box -->
                        <div class="info-box">
                            <!-- Apply any bg-* class to to the icon to color it -->
                            <span class="info-box-icon bg-blue"><i class="fa fa-bell"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Likes</span>
                                <span class="info-box-number">93,139</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-6 col-xs-12">
                        <!-- Apply any bg-* class to to the info-box to color it -->
                        <div class="info-box bg-red">
                            <span class="info-box-icon"><i class="fa fa-calculator"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Likes</span>
                                <span class="info-box-number">41,410</span>
                                <!-- The progress section is optional -->
                                <div class="progress">
                                    <div class="progress-bar" style="width: 70%"></div>
                                </div>
                                <span class="progress-description">
                                  70% Increase in 30 Days
                                </span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <div class="col-sm-3 col-xs-6">
                        <div class="box box-solid box-primary">
                            <div class="box-header">
                                <h3 class="box-title">Primary Solid Box</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                The body of the box
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                    <div class="col-sm-3 col-xs-6">
                        <div class="box box-solid box-success">
                            <div class="box-header">
                                <h3 class="box-title">Success Solid Box</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">
                                The body of the box
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <div class="box-footer">
    </div>
    <!-- /.box-footer-->
</div>
@endsection
