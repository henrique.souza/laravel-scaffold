@extends('adminlte::auth')

@section('body_class', 'login-page')

@section('body')
    <div class="login-box" style="box-shadow: #001a35">
        <div class="login-logo" style="background-color: #d2d6de; ">
            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}">{!! config('adminlte.logo_2', '<b>Admin</b>LTE') !!}</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <h3 class="text-center margin-bottom">{{ env('APP_SIGLA') . ' - ' . env('APP_NAME') }}</h3>
            <form action="{{ url(config('adminlte.login_url', 'login')) }}" method="post">
                {{ csrf_field() }}

                <div class="form-group has-feedback {{ $errors->has('matricula') ? 'has-error' : '' }}">
                    <input type="matricula" name="matricula" class="form-control" value="{{ old('matricula') }}"
                           placeholder="{{ trans('adminlte::adminlte.matricula') }}">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    @if ($errors->has('matricula'))
                        <span class="help-block">
                            <strong>{{ $errors->first('matricula') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <input type="password" name="password" class="form-control"
                           placeholder="{{ trans('adminlte::adminlte.password') }}">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="row">
                    <div class="col-xs-8">
                    </div>
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-success btn-block btn-flat">
                            {{ trans('adminlte::adminlte.sign_in') }}
                        </button>
                    </div>
                </div>
            </form>
            <div style="padding-bottom: 10px;">
                <p>
                    <a href="{{ url(config('adminlte.password_reset_url', 'password/reset')) }}" class="text-center">
                        {{ trans('adminlte::adminlte.i_forgot_my_password') }}
                    </a>
                </p>
            </div>
            <div role="alert" class="alert" style="background-color: #dee5f3;">
                <p>
                    Versão: {{ $version }}
                </p>
                <p>
                    Última Atualização: 28/05/2021
                </p>
                @if (env('APP_ENV') != 'production')
                    <p>
                        Revisão: <a href="{{ $revision }}" target="_blank" style="color: #3c8dbc">{{ $commit }}</a>
                    </p>
                @endif
            </div>
        </div>
        <!-- /.login-box-body -->
    </div><!-- /.login-box -->
@stop