@extends('adminlte::master')

@section('adminlte_css')
    @yield('css')
@stop

@section('adminlte_js')
    @yield('js')
    <script>
        let bgpath = "{{ asset('images/backgrounds') }}";
        let imgs = [];
        for (let i = 1; i <= 10; i++) {
            imgs.push(`${bgpath}/${i}.jpg`);
        }
        $.backstretch(imgs, {duration: 3000, fade: 750});
    </script>
@stop