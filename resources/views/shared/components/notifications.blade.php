<meta name="csrf-token" content="{{ csrf_token() }}">

<ul class="nav navbar-nav">
    <li class="dropdown notifications-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-bell"></i>
            @if( $notifications > 0 )
            <span id="notifications-total" class="label label-warning">
                {{ $notifications }}
            </span>
            @endif
        </a>
        <ul class="dropdown-menu">
            <li id="dropdown-menu-header" class="header">Você não possui notificações</li>
            <li>
                <!-- inner menu: contains the actual data -->
                <ul id="notifications-list" class="menu">
                    <li class="notification-item dropdown-item">
                    </li>
                </ul>
            </li>
            <!--li class="footer"><a href="#">Ver tudo</a></li-->
        </ul>
    </li>
</ul>