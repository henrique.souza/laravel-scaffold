$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(() => {
    setInterval(pullTotalNaoLido, 60000);
    pullLista();
});

let pullTotalNaoLido = () => {
    $.get('notificacoes/total-nao-lidas', data => {
        let totalNaoLido = data.total_nao_lido;
        setTotalNaoLido(totalNaoLido);
        pullLista();
    });
}

let pullLista = () => {
    $.get('notificacoes', data => {

        $('#notifications-list').html('');
        data.forEach(item => {
            let html = htmlItemLista(item);
            $('#notifications-list').append(html);
        });
        $(".notification-item>a")
            .prop("onclick", null)
            .off("click")
            .click(ler);
        $(".btn-rm-notification")
            .prop("onclick", null)
            .off("click")
            .click(ocultar);

        setMsgLista();
    });
}

let ler = (event) => {

    event.preventDefault();

    let element = event.currentTarget;
    let cdNotificacao = element.id;

    if (! $(element).hasClass("text-bold")) {
        return false;
    }

    $.ajax({
        url: `notificacoes/${cdNotificacao}/ler`,
        type: 'PUT',
        success: () => {
            setMsgLista();
            $(element).removeClass("text-bold");
            setTotalNaoLido();
        }
    });

    return false;
}

let ocultar = (event) => {

    event.stopPropagation();
    event.preventDefault();

    let element = event.currentTarget;
    let parent = $(element).closest('li');
    let cdNotificacao = element.id.replace("_", "");

    parent.hide("slow", () => {
        parent.remove();
        setMsgLista();
        setTotalNaoLido();
    });

    $.ajax({
        url: `notificacoes/${cdNotificacao}/ocultar`,
        type: 'PUT'
    });
}

let htmlItemLista = (item) => {

    let itemClass = item.fl_lida ? "" : "text-bold";
    let iconColor = item.fl_lida ? "text-gray" : "text-aqua";
    let iconType = "fa-paper-plane";
    let dsNotificacao = `${item.no_sigla_sistema_origem} - ${item.ds_notificacao}`;
    let cdNotificacao = item.cd_notificacao;

    let html = `<li class="notification-item dropdown-item inline">
        <a href="#" id="${cdNotificacao}" class="${itemClass}" data-toggle="tooltip" title="${dsNotificacao}">
            <i class="fa mr-2 ${iconType} ${iconColor}"></i>
            <span class="notification-msg">${dsNotificacao}</span>
            <button id="_${cdNotificacao}" class="btn btn-xs btn-danger right float-right btn-rm-notification">ocultar</button>
            <span class="right text-muted text-sm float-right" style="margin-right: 6px">
                <i class="fa fa-clock text-gray mr-2 pl-1"></i>
                ${item.intervalo}
            </span>
        </a>
    </li>`;

    return html
}

let setTotalNaoLido = (qtd = null) => {
    qtd = qtd ?? $(".notification-item>a.text-bold").length;
    let text = qtd > 0 ? qtd : '';
    $('#notifications-total').text(text);
}

let setMsgLista = (qtd = null) => {
    qtd = qtd ?? $("li.notification-item").length;
    const msg = qtd > 0 ? "Você possui %s notificações." : "Você não possui notificações";
    $('#dropdown-menu-header').text(msg.replace('%s', qtd));
}