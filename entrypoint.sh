#!/bin/bash

VENDOR_DIR=$APP_PATH/vendor
if [ ! -d "$APP_PATH/vendor" -o ! -f "$VENDOR_DIR/autoload.php" ]; then
  echo "  ___                           _      _     ^ "
  echo " / __|__ _ _ __  __ _ ___ _ __ (_)_ _ (_)   / \ "
  echo "| (__/ _  | '_ \/ _  / -_) '  \| | ' \| | (     ) "
  echo " \___\__,_| .__/\__, \___|_|_|_|_|_||_|_|   /_\  "
  echo "          |_|   |___/                            "
  echo "Iniciando deploy..."
  cd $APP_PATH
  echo "Instalando dependências do backend..."
  composer install
fi
if [ ! -d "$APP_PATH/node_modules" ]; then
  echo "Instalando dependências do frontend..."
  npm install
fi
ENV_FILE=$APP_PATH/.env
if [ ! -f "$ENV_FILE" ]; then
    echo "** Copiando arquivo de variáveis de ambiente..."
    cp $APP_PATH/.env.local $ENV_FILE
fi
cd $APP_PATH
echo "** Iniciando servidor http..."
/bin/bash -c "/usr/sbin/apache2ctl -D FOREGROUND"
echo "** Deploy concluído"