<?php
/**
 * Created by PhpStorm.
 * User: Administrador
 * Date: 05/05/2021
 * Time: 23:20
 */

namespace App\Auth;

use App\Models\UsuarioEsqueceuSenha;
use Illuminate\Auth\Passwords\DatabaseTokenRepository as BaseDatabaseTokenRepository;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Carbon;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;

/**
 * @property BaseDatabaseTokenRepository tokenRepository
 */
class DatabaseTokenRepository extends BaseDatabaseTokenRepository
{
    /**
     * Create a new token repository instance.
     *
     * @param  \Illuminate\Database\ConnectionInterface  $connection
     * @param  \Illuminate\Contracts\Hashing\Hasher  $hasher
     * @param  string  $table
     * @param  string  $hashKey
     * @param  int  $expires
     * @return void
     */
    public function __construct(ConnectionInterface $connection, HasherContract $hasher, $table, $hashKey, $expires = 60)
    {
        parent::__construct($connection, $hasher, $table, $hashKey, $expires);
    }

    /**
     * Create a new token record.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @return string
     */
    public function create(CanResetPasswordContract $user)
    {
        $this->getTable()->where('cd_usuario', $user->cd_usuario)->delete();

        // We will create a new, random token for the user so that we can e-mail them
        // a safe link to the password reset form. Then we will insert a record in
        // the database so that we can verify the token within the actual reset.
        $token = $this->createNewToken();

        $this->getTable()->insert([
            'cd_usuario' => $user->cd_usuario,
            'ds_hash_link' => $this->hasher->make($token),
            'dt_cadastro' => new Carbon,
        ]);

        return $token;
    }

    /**
     * Determine if a token record exists and is valid.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword $user
     * @param  string $token
     * @return bool
     */
    public function exists(CanResetPasswordContract $user, $token)
    {
        $record = (array) $this->getTable()->where(
            'cd_usuario', $user->cd_usuario
        )->first();

        return $record &&
            ! $this->tokenExpired($record['dt_cadastro']) &&
            $this->hasher->check($token, $record['ds_hash_link']
            );
    }

    /**
     * Delete a token record.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword $user
     * @return void
     */
    public function delete(CanResetPasswordContract $user)
    {
        $this->getTable()->where('cd_usuario', $user->cd_usuario)->delete();
    }

    /**
     * Delete expired tokens.
     *
     * @return void
     */
    public function deleteExpired()
    {
        parent::deleteExpired();
    }
}