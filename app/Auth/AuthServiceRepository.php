<?php
/**
 * Created by PhpStorm.
 * User: Administrador
 * Date: 06/05/2021
 * Time: 14:07
 */

namespace App\Auth;

use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use GuzzleHttp\Client;

/**
 * @property \SoapClient soapClient
 * @property Client apiClient
 */
class AuthServiceRepository
{
    const K_DEFAULT_PASSWORD = "Terracap_#$%s";

    /**
     * AuthClientService constructor.
     * @param Client $restClient
     */
    public function __construct(Client $restClient)
    {
        $this->soapClient = new \SoapClient(env('SOA_AUTH_SERVICE'));
        $this->apiClient = $restClient;
    }

    /**
     * Redefine a senha do usuário
     *
     * @param User $user
     * @return string
     * @throws AuthorizationException
     */
    public function redefinePassword(User $user)
    {
        $nrCpf = $user->pessoaRh->nr_cpf;
        $response = $this->soapClient->recuperarSenha([
            'matricula' => strtoupper($user->ds_login),
            'cpf' => $nrCpf,
            'loginSolicitante' => strtoupper($user->ds_login),
        ]);
        if($response->control->description == 'Erro') {
            throw new AuthorizationException("Erro ao recuperar a senha: {$response->control->detail}");
        }

        return sprintf(self::K_DEFAULT_PASSWORD, ltrim($nrCpf, '0'));
    }

    /**
     * Altera a senha do usuário
     *
     * @param User $user
     * @param string $old
     * @param string $new
     * @return User
     * @throws AuthorizationException
     */
    public function resetPassword(User $user, string $old, string $new)
    {
        $response = $this->soapClient->alterarSenha([
            'matricula' => strtoupper($user->ds_login),
            'senha' => $old,
            'senhaNova' => $new,
            'confirmaSenhaNova' => $new,
        ]);
        if($response->control->description == 'Erro') {
            throw new AuthorizationException("Erro ao redefinir a senha: {$response->control->detail}");
        }
        $token = $this->getApiToken($user->ds_login, $new);

        $user->setApiTokenAttribute($token->access_token);

        return $user;
    }

    /**
     * Recupera o token da API Interna a partir das credenciais do usuário
     *
     * @param $matricula
     * @param $password
     * @param bool $usuario
     * @return mixed
     * @throws AuthorizationException
     */
    public function getApiToken($matricula, $password, $usuario = true)
    {
        $params = [
            'matricula' => strtoupper($matricula),
            'senha' => $password,
            'sistema' => env('APP_CODE'),
            'usuario' => $usuario,
        ];
        $token = $this->apiClient->post(
            env('API_INTERNA_URL') . '/GIA/tokens/get',
            ['json' => $params]
        )->getBody()->getContents();
        $token = json_decode($token);

        if (!isset($token->access_token)) {
            throw new AuthorizationException('Erro ao recuperar token de autenticação do usuário.');
        }

        return $token;
    }

    /**
     * Recupera a árvore de menus do usuário para montagem do Menu LTE
     *
     * @param $token
     * @return mixed
     */
    public function getMenuLte($token)
    {
        $menuLte = $this->apiClient->get(
            env('API_INTERNA_URL') . '/GIA/menus/lte/usuario',
            [ 'headers' => ['Authorization' => "Bearer {$token}"] ]
        )->getBody()->getContents();

        return json_decode($menuLte, 'true')['menu_lte'];
    }
}