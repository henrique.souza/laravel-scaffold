<?php

namespace App\Providers;

use GuzzleHttp\Client;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Auth\Events\Login;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @param Dispatcher $events
     * @param Client $client
     * @return void
     */
    public function boot(Dispatcher $events, Client $client)
    {
        $this->registerPolicies();

        $events->listen(Login::class, function (Login $auth) use ($client, $events) {
            $cdSistema = env('APP_CODE');
            $access = $client->get(
                env('API_INTERNA_URL') . "/GIA/acessos/sistema/$cdSistema/itens"
            )->getBody()->getContents();
            $fullItems = json_decode($access)->itens;

            $token = $auth->user->api_token;
            session([
                'access_token' => $token,
                'pessoa_descricao' => $auth->user->pessoa_descricao,
            ]);
            $userAccess = $client->get(
                env('API_INTERNA_URL') . "/GIA/acessos/usuario/itens",
                ['headers' => ['Authorization' => "Bearer {$token}"]]
            )->getBody()->getContents();
            $userItems = json_decode($userAccess)->itens;

            foreach ($fullItems as $item) {
                $noReferenciaItem = $item->no_referencia_item;
                Gate::define($noReferenciaItem, function () use ($userItems, $noReferenciaItem) {
                    return collect($userItems)->contains(function ($userItem) use ($noReferenciaItem) {
                        return $userItem->no_referencia_item == $noReferenciaItem;
                    });
                });
            }
        });

        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
            if ($menuLte = session('menu_lte')) {
                foreach ($menuLte as $menu) {
                    $event->menu->add($menu);
                }
            }
        });
    }
}
