<?php

namespace App\Models;

use App\Services\EmailService;
use http\Exception\InvalidArgumentException;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property mixed ds_senha
 * @property mixed pessoaRh
 * @property mixed ds_login
 * @property EmailService mailService
 */
class User extends Authenticatable
{
    use Notifiable;

    use CanResetPassword;

    /**
     * @var string
     */
    protected $table = 'tc_usuario';
    /**
     * @var string
     */
    protected $primaryKey = 'cd_usuario';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'ds_senha', 'ds_senha2', 'ds_senha3'
    ];

    /**
     * @param string $apiToken
     */
    public function setApiTokenAttribute(string $apiToken)
    {
        $this->attributes['api_token'] = $apiToken;
    }

    /**
     * @param null $pessoaDescricao
     */
    public function setPessoaDescricaoAttribute($pessoaDescricao = null)
    {
        $this->attributes['pessoa_descricao'] = $pessoaDescricao ?? $this->getPessoaDescricao();
    }

    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmailForPasswordReset()
    {
        return strtolower($this->pessoaRh->ds_email ?: $this->pessoaRh->ds_email_gac);
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->ds_senha;
    }

    /**
     * Retorna matrícula seguida do nome do usuário.
     *
     * @return string
     */
    public function getPessoaDescricao()
    {
        return "{$this->ds_login} - {$this->pessoaRh->nm_pessoa}";
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function pessoaRh()
    {
        return $this->hasOne(PessoaRh::class, 'cd_pessoa_rh', 'cd_pessoa_rh');
    }

    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        if (!$this->getEmailForPasswordReset())
            throw new InvalidArgumentException('Usuário não possui E-mail cadastrado.');

        // TODO Ver injeção de dependência
        $mailService = new EmailService();
        $mailService->sendEmailRedefinirSenha($this->getEmailForPasswordReset(), $token);
    }

}
