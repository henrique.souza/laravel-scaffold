<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PessoaRh extends Model
{

    protected $connection = 'oracle-gap';

    protected $table = 'tc_pessoa_rh';

    protected $primaryKey = 'cd_pessoa_rh';

    // Relationships
}
