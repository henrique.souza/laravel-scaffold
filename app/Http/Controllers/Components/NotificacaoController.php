<?php

namespace App\Http\Controllers\Components;

use App\Services\NotificacaoService;
use App\Http\Controllers\Controller;

/**
 * @property NotificacaoService service
 */
class NotificacaoController extends Controller
{
    /**
     * NotificacaoController constructor.
     * @param NotificacaoService $service
     */
    public function __construct(NotificacaoService $service)
    {
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTotalNaoLidas()
    {
        $token = session('access_token');
        $response = $this->service->getTotalNaoLidas($token);
        return response()->json($response);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function get()
    {
        $token = session('access_token');
        $response = $this->service->getAll($token);
        return response()->json($response);
    }

    /**
     * @param int $cdNotificacao
     * @param string $acao
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeStatus(int $cdNotificacao, string $acao)
    {
        $token = session('access_token');
        $response = $this->service->changeStatus($token, $cdNotificacao, $acao);
        return response()->json($response);
    }
}
