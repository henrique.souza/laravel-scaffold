<?php

namespace App\Http\Controllers\Auth;

use App\Auth\AuthServiceRepository;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\PasswordReset;

/**
 * @property AuthServiceRepository authClient
 */
class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @param AuthServiceRepository $authClient
     */
    public function __construct(AuthServiceRepository $authClient)
    {
        $this->middleware('guest');
        $this->middleware('email')->only('reset');
        $this->authClient = $authClient;
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'matricula' => 'required',
            'cpf' => 'required',
            'password' => 'required|confirmed|min:8',
        ];
    }

    /**
     * Get the password reset credentials from the request.
     *
     * @param Request $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return [
            'ds_login' => $request->matricula,
            'password' => $request->password,
            'password_confirmation' => $request->password_confirmation,
            'token' => $request->token,
        ];
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword $user
     * @param  string $password
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    protected function resetPassword($user, $password)
    {
        $oldPassword = $this->authClient->redefinePassword($user);
        $user = $this->authClient->resetPassword($user, $oldPassword, $password);
        $token = $this->authClient->getApiToken($user->ds_login, $password);
        $menuLte = $this->authClient->getMenuLte($token->access_token);

        // TODO - Mover para o evento de Login
        session(['menu_lte' => $menuLte]);
        session(['access_token' => $token->access_token]);

        $user->setApiTokenAttribute($token->access_token);
        $user->setPessoaDescricaoAttribute();

        event(new PasswordReset($user));

        $this->guard()->login($user);
    }
}
