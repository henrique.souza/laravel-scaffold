<?php

namespace App\Http\Controllers\Auth;

use App\Auth\AuthServiceRepository;
use App\Http\Controllers\Controller;
use App\Models\User;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Events\Dispatcher;

/**
 * @property AuthServiceRepository authClient
 */
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @param AuthServiceRepository $authClient
     */
    public function __construct(AuthServiceRepository $authClient)
    {
        $this->middleware('guest')->except('logout');
        $this->authClient = $authClient;
    }


    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        // TODO - criar helpers para essas funções de que recuperam a versão e utilizar direto na view. Depois remover este método
        $version = explode('-', \Tremby\LaravelGitVersion\GitVersionHelper::getVersion())[0];
        //$commit = (new \PragmaRX\Version\Package\Version())->commit();
        $commit = '827ce20022c22a2ad4ad5939740df3f465d86b33';
        $revision = env('VERSION_GIT_REMOTE_REPOSITORY') . "/commit/$commit";

        return view('auth.login')->with([
            'version' => $version,
            'commit' => substr($commit, 0, 6),
            'revision' => $revision,
        ]);
    }

    /**
     * Handle a login request to the application.
     *
     * @param Request $request
     * @param Dispatcher $events
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function login(Request $request, Dispatcher $events)
    {
        // TODO - Mover para arquitetura
        $this->validateLogin($request);

        try {
            $token = $this->authClient->getApiToken($request->matricula, $request->password);
            $menuLte = $this->authClient->getMenuLte($token->access_token);

            // TODO - Mover para o evento de Login
            session(['menu_lte' => $menuLte]);
            session(['access_token' => $token->access_token]);

        } catch (ClientException $e) {
            if ($e->getCode() === 401)
                return $this->sendFailedLoginResponse($request);
            throw $e;
        }
        $user = User::with('pessoaRh')
            ->findOrFail($token->usuario->cd_usuario);
        $user->setApiTokenAttribute($token->access_token);
        $user->setPessoaDescricaoAttribute();

        Auth::login($user);

        return redirect($this->redirectTo);
    }

    /**
     * @return string
     */
    public function username()
    {
        return 'matricula';
    }


}
