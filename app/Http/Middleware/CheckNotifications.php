<?php

namespace App\Http\Middleware;

use App\Services\NotificacaoService;
use Closure;
use Illuminate\Support\Facades\View;

/**
 * @property NotificacaoService service
 */
class CheckNotifications
{
    /**
     * CheckNotifications constructor.
     * @param NotificacaoService $service
     */
    public function __construct(NotificacaoService $service)
    {
        $this->service = $service;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param NotificacaoService $service
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = session('access_token');

        $response = $this->service->getTotalNaoLidas($token);
        View::share('notifications', $response->total_nao_lido);

        return $next($request);
    }
}
