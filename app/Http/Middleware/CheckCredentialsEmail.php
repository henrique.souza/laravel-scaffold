<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use \InvalidArgumentException;

class CheckCredentialsEmail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = User::whereRaw(sprintf("UPPER(ds_login) = '%s'", strtoupper($request->matricula)))
                ->first();
            $this->validate($request, $user);

            $request->merge(['email' => $user->getEmailForPasswordReset()]);

            return $next($request);

        } catch (InvalidArgumentException $e) {
            return redirect()->back()->withErrors(['credentials' => $e->getMessage()]);
        }
    }

    /**
     * @param $request
     * @param $user
     * @throws InvalidArgumentException
     * @return void
     */
    protected function validate($request, $user)
    {
        $request->validate([
            'matricula' => 'required',
            'cpf' => 'required',
        ]);
        if (!$user) {
            throw new InvalidArgumentException('Matrícula inválida!');
        }
        if ($user->pessoaRh->nr_cpf != $request->cpf) {
            throw new InvalidArgumentException('CPF Inválido!');
        }
        $email = $user->getEmailForPasswordReset();
        if (!$email) {
            throw new InvalidArgumentException('Usuario não possui e-mail para envio do link. Solicite atualização do cadastro!');
        }
    }
}
