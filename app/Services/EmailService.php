<?php
/**
 * Created by PhpStorm.
 * User: Administrador
 * Date: 25/06/2021
 * Time: 22:30
 */

namespace App\Services;

use GuzzleHttp\Client;

/**
 * @property Client apiClient
 */
class EmailService
{
    /**
     * NotificacaoService constructor.
     */
    public function __construct()
    {
        $this->apiClient = new Client();
    }

    /**
     * Cria email para redefinição de senha
     *
     * @param string $destinatario
     * @param string $token
     * @return void
     */
    public function sendEmailRedefinirSenha(string $destinatario, string $token)
    {
        $link = url(route('password.reset', ['token' => $token, 'email' => $destinatario], false));

        $params = [
            'destinatario' => $destinatario,
            'cd_sistema_origem' => env('APP_CODE'),
            'ds_link' => $link,
        ];
        $this->apiClient->post(
            env('API_INTERNA_URL') . '/GIA/email/redefinir-senha',
            ['json' => $params]
        )->getBody()->getContents();
    }
}