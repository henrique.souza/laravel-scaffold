<?php
/**
 * Created by PhpStorm.
 * User: Administrador
 * Date: 13/06/2021
 * Time: 20:05
 */

namespace App\Services;

use GuzzleHttp\Client;
use http\Exception\InvalidArgumentException;

/**
 * @property Client apiClient
 */
class NotificacaoService
{
    const K_ACAO_LER = 'ler';
    const K_ACAO_OCULTAR = 'ocultar';
    const K_ENDPOINT_NOTIFICACOES = '/GAP/notificacoes/';

    /**
     * NotificacaoService constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->apiClient = $client;
    }

    /**
     * @param string $token
     * @return string
     */
    public function getTotalNaoLidas(string $token)
    {
        return $this->callGetService($token, 'total');
    }

    /**
     * @param string $token
     * @return string
     */
    public function getAll(string $token)
    {
        return $this->callGetService($token);
    }

    /**
     * @param string $token
     * @param int $cdNotificacao
     * @param string $action
     * @return string
     */
    public function changeStatus(string $token, int $cdNotificacao, string $action)
    {
        if (! in_array($action, [ self::K_ACAO_LER, self::K_ACAO_OCULTAR ])) {
            throw new InvalidArgumentException("Ação \"$action\" inexistente.");
        }
        $response = $this->apiClient->put(
            env('API_INTERNA_URL') . self::K_ENDPOINT_NOTIFICACOES . "$cdNotificacao/$action",
            [ 'headers' => ['Authorization' => "Bearer {$token}"] ]
        )->getBody()->getContents();

        return $response;
    }

    /**
     * @param string $token
     * @param string $endpoint
     * @return string
     */
    protected function callGetService(string $token, string $endpoint = '')
    {
        $response = $this->apiClient->get(
            env('API_INTERNA_URL') . self::K_ENDPOINT_NOTIFICACOES . "usuario-sistema/$endpoint",
            [ 'headers' => ['Authorization' => "Bearer {$token}"] ]
        )->getBody()->getContents();

        return json_decode($response);
    }
}