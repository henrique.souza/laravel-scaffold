<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

# Scaffold de Aplicações Laravel

O Scaffold de aplicações Laravel da Terracap traz todo o estrutura necessária para iniciar uma aplicação do zero, contendo prontos os compontentes de: autenticação/autorização, menu principal, tratamento de erros, folhas de estilo AdminLTE/Bootstrap e suporte à criação de APIs com Swagger.
<br/>Para iniciar uma aplicação Laravel, basta executar os passos na ordem a seguir: 
1. Clonar este projeto em seu workspace informando no final o nome da aplicação. Ex.:  
```
git clone git@sv56.terracapnet.local:php-projects/laravel-scaffold.git nome-aplicacao  
```
2. Criar o novo sistema no GIA de homologaçao em <a href="https://gia-homol">https://gia-homol</a>
3. Alterar no arquivo .env as variáveis APP_NAME, APP_SIGLA e APP_CODE.
4. Alterar no arquivo .env as veriáveis DB_USERNAME e DB_PASSWORD.
5. Executar os passos para instalação descritos a seguir.

## Instalação

1. Instale o [docker](https://docs.docker.com/engine/installation/) seguindo as instruções na página oficial.
<br/><br/>*Ps.: caso sua versão do windows não suporte a instalação padrão do Docker, faça o download do "Docker Toolbox for Windows" [aqui](https://drive.google.com/file/d/1r5GhWYxyetFrn0Dxw8KNeV6t6b5M6Nab/view?usp=sharing).*

2. Clone o projeto: 
```
git clone git@sv56.terracapnet.local:php-projects/laravel-scaffold.git nome-aplicacao
```
</br>*Ps.: baixe o projeto em alguma pasta abaixo do seu usuário (Ex.: C:\Users\Administrador\projects).*

3. Dentro da pasta do projeto, inicie a aplicação através do comando:
```
cd nome-projeto
```
```
docker-compose up --build
```
</br>*Ps.: esta rotina leva alguns minutos, pois além de baixar as imagens do ambiente irá instalar as dependências da aplicação (composer) e fazer todas as configurações iniciais.

4. Acesse o ambiente local
<br/>[http://localhost:8081](http://localhost:8081)

## Utilização

- Iniciar a aplicação:
```
docker-compose up -d
```

- Acessar e interagir com o container:
```
docker-compose exec app bash
``` 

- Executar comandos dentro do container. Ex.: atualização das dependências
```
docker-compose exec app composer update
``` 

- Executar testes de unidade
```
docker-compose exec app vendor/bin/phpunit
``` 

- Remover o container sempre após a utilização:
```
docker-compose down
``` 

## Links Externos

- [Laravel 5.8](https://laravel.com/docs/5.8).
- [AdminLTE Template](https://adminlte.io/themes/v3/).
- [Swagger API](https://github.com/zircote/swagger-php).