const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js/app.min.js')
    .js('public/vendor/adminlte/dist/js/adminlte.js', 'public/vendor/adminlte/dist/js/adminlte.min.js')
    .js('public/js/components/notifications.js', 'public/js/components/notifications.min.js')
    .css('public/vendor/adminlte/dist/css/AdminLTE.css', 'public/vendor/adminlte/dist/css/AdminLTE.min.css')
    .sass('resources/sass/app.scss', 'public/css');